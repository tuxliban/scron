scron
=====
Demonio cron minimalista

Características
--------
* Programar comandos para que se ejecuten en fechas y horas específicas
* Demonio único y archivo de configuración
* Salida del registro de trabajo: 'comando & >> /var/log/cron.log'
* Ejecute el trabajo como un usuario diferente: 'su -c 'comando' usuario'
* Iniciar sesión en stdout o syslog
* Sin soporte por correo

Configuración
-------------
```
#########################################################################
# Minuto (0-59)                                                         #
# |     Hora (0-23)                                                     #
# |     |       Día del mes (1-31)                                      #
# |     |       |       Mes (1-12)                                      #
# |     |       |       |       Día de la semana (0-6 donde 0=Domingo)  #
# |     |       |       |       |       Comandos                        #
# *     *       *       *       *       *                               #
#########################################################################
```

Ejemplo de archivo crontab:

```
# Actualizar la base de datos para mlocate a las 6:00 todos los días
 0	*	*	*	*	updatedb

# A las 10 pm todos los días de la semana de lunes a viernes hacer un backup
 0	22	*	*	1-5	rclone sync -P $HOME/box_cloud Box:
 
# A las 5 am del tercer día del mes, ejecutar un script como el usuario *loser*
 0	5	*/3	*	*	su -c foo.sh loser
```